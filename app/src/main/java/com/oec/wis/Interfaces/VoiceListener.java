package com.oec.wis.Interfaces;

/**
 * Created by asareri12 on 08/03/17.
 */

public interface VoiceListener {

    public void onPlayButtonClicked(String filePath);
}
