//package com.oec.wis.adapters;
//
//import android.app.Activity;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.graphics.Typeface;
//import android.support.v7.app.AlertDialog;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import com.android.volley.AuthFailureError;
//import com.android.volley.Response;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.ImageLoader;
//import com.android.volley.toolbox.JsonObjectRequest;
//import com.oec.wis.ApplicationController;
//import com.oec.wis.R;
//import com.oec.wis.dialogs.PubDetails;
//import com.oec.wis.entities.WISAds;
//import com.oec.wis.tools.Tools;
//
//import org.joda.time.DateTime;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Locale;
//import java.util.Map;
//
//public class MyAdsAdapter extends BaseAdapter {
//    Context context;
//    List<WISAds> data;
//
//    public MyAdsAdapter(Context context, List<WISAds> data) {
//        this.data = data;
//        this.context = context;
//    }
//
//    @Override
//    public int getCount() {
//        return data.size();
//    }
//
//    @Override
//    public Object getItem(int position) {
//        return data.get(position);
//    }
//
//    @Override
//    public long getItemId(int position) {
//        return data.get(position).getId();
//    }
//
//    @Override
//    public View getView(final int position, View convertView, ViewGroup parent) {
//        final ViewHolder holder;
//        if (convertView == null) {
//            convertView = ((LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.row_my_ads, null);
//            holder = new ViewHolder(convertView);
//        } else {
//            holder = (ViewHolder) convertView.getTag();
//        }
//        holder.ivPhoto.setImageResource(0);
//
//
//        holder.tvTitle.setText(data.get(position).getTitle());
//
//        System.out.println("data values" +data.get(position).toString());
//        System.out.println("description" +data.get(position).getDesc());
//
//
//
////        Created date2017-02-02 09:32:15
//
//
//
//        String cDate=data.get(position).getcDate();
//        String lDate =data.get(position).getlDate();
//
//        System.out.println("Created date" +cDate   +"last date"    +lDate);
//
//        String desc=data.get(position).getDesc();
//        System.out.println("description" +desc);
//
//
//
////        holder.tvUserDesc.setText(data.get(position).getDesc());
////
////
////        try{
////            String desc=data.get(position).getDesc();
////            Log.d("desc",desc);
////            Log.d("data",data.toString());
////
////            holder.tvUserDesc.setText(data.get(position).getDesc());
////        }catch (NullPointerException ex){
////            System.out.println("ex"+ex.getMessage());
////        }
//
//
//        Typeface font= Typeface.createFromAsset(context.getAssets(), "fonts/Harmattan-Regular.ttf");
//        holder.tvTitle.setTypeface(font);
//        holder.tvDate.setTypeface(font);
//        holder.tvUserDesc.setTypeface(font);
//        holder.tvStatus.setTypeface(font);
//
//
////        if (data.get(position).getStatus() == 1) {
////            holder.tvStatus.setText(context.getString(R.string.progress));
//////            holder.llContent.setBackgroundResource(R.drawable.llayout3);
////        } else {
////            holder.tvStatus.setText(context.getString(R.string.expired));
//////            holder.llContent.setBackgroundResource(R.drawable.llayout5);
////        }
////        holder.tvStatus.setText("31/1/2017");
//
//
////        holder.trashBtn.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View v) {
////
////                deleteDialog(position);
////            }
////        });
////
//
//       holder.llContent.setOnClickListener(new View.OnClickListener() {
//           @Override
//           public void onClick(View v) {
//               Intent it=new Intent(context,PubDetails.class);
//               context.startActivity(it);
//           }
//       });
//
//        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
//        SimpleDateFormat format_two = new SimpleDateFormat("dd/MM/yy");
//        SimpleDateFormat format_three = new SimpleDateFormat("dd/MMMM/yyyy");
//        SimpleDateFormat hour = new SimpleDateFormat("H");
//        SimpleDateFormat min = new SimpleDateFormat("mm");
//        SimpleDateFormat sec = new SimpleDateFormat("ss");
//        Date d = null;
//        String h="",m="",s="";
//        String created_at = null;
//        System.out.println("format" +format);
//        try {
//            d = format.parse(data.get(position).getcDate());
//            created_at  =format_two.format(d);
//            h = hour.format(d);
//            m=min.format(d);
//            s=sec.format(d);
//
//            String text = h.concat("H").concat(m).concat("'").concat(s);
//            String datetext=h.concat(":").concat(m);
//            System.out.println("date====>" +datetext    + "text" +text);
//            System.out.println("date value get response" +data.get(position).getcDate());
//
////            holder.tvDate.setText(datetext);
//
//
//////            down vote
////            Calendar now = Calendar.getInstance();
////            if(now.get(Calendar.AM_PM) == Calendar.AM){
////                // AM
////                holder.tvDate.setText(datetext).now.get(Calendar.HOUR)+":AM");
////
////                System.out.println(""+now.get(Calendar.HOUR)+":AM");
////            }else {
////                //
//////                holder.tvDate.setText(datetext);
////                holder.tvDate.setText(datetext + now.get(Calendar.HOUR)+":PM");
////
////
////                System.out.println("" + now.get(Calendar.HOUR) + ":PM");
////
////            }
//            // Get date from string
//            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            Date date = dateFormatter.parse(data.get(position).getcDate());
//
//// Get time from date
//            SimpleDateFormat timeFormatter = new SimpleDateFormat("h:mm a");
//
//
//
//            String displayValue = timeFormatter.format(date);
//          String timeStr = displayValue.toLowerCase(Locale.UK);/*small letere*/
////            String exactvalue = displayValue.replace("AM", "am").replace("PM","pm");
//
//            System.out.println("display value" +displayValue);
//            System.out.println("display exactvalue" +timeStr);
//            holder.tvDate.setText(timeStr);
//
//// Done!
//
//            } catch (ParseException e) {
//            e.printStackTrace();
//
//        }
//
//
//
////
//        holder.tvStatus.setText(created_at);
//        if (data.get(position).getTypeObj().equals("video")) {
//            ApplicationController.getInstance().getImageLoader().get(context.getString(R.string.server_url4) + data.get(position).getThumb(), new ImageLoader.ImageListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    holder.ivPhoto.setImageResource(R.drawable.empty);
//                }
//
//                @Override
//                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
//                    if (response.getBitmap() != null) {
//                        holder.ivPhoto.setImageBitmap(response.getBitmap());
//                    } else {
//                        holder.ivPhoto.setImageResource(R.drawable.empty);
//                    }
//                }
//            });
//        } else {
//            ApplicationController.getInstance().getImageLoader().get(context.getString(R.string.server_url3) + data.get(position).getPhoto(), new ImageLoader.ImageListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    holder.ivPhoto.setImageResource(R.drawable.empty);
//                }
//
//                @Override
//                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
//                    if (response.getBitmap() != null) {
//                        holder.ivPhoto.setImageBitmap(response.getBitmap());
//                    } else {
//                        holder.ivPhoto.setImageResource(R.drawable.empty);
//                    }
//                }
//            });
//        }
////        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
////        Date d = null;
////        try {
////            d = format.parse(data.get(position).getcDate());
////            Date date =format.parse(data.get(position).getEndTime());
////
////            Log.d("date 1111" , String.valueOf(date));
////
////            Log.d("date" , String.valueOf(d));
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
//////        String rDate = net.danlew.android.joda.DateUtils.getRelativeTimeSpanString(context, new DateTime(d), true).toString();
////        String rDate = net.danlew.android.joda.DateUtils.getRelativeTimeSpanString(context, new DateTime(d), true).toString();
////
////        holder.tvDate.setText(rDate);
//
//        return convertView;
//    }
//
//
//    @Override
//    public int getViewTypeCount() {
//        if (data.size() > 1)
//            return data.size();
//        else
//            return 1;
//    }
//
//
//    private void deleteDialog(final int position) {
//        new AlertDialog.Builder(context)
//                .setMessage(context.getString(R.string.msg_confirm_delete_post))
//                .setPositiveButton(context.getString(R.string.yes), new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        removePubs(position);
//                    }
//
//                })
//                .setNegativeButton(context.getString(R.string.no), null)
//                .show();
//    }
//
//    private void removePubs(final int position) {
//        JSONObject jsonBody = new JSONObject();
//
//        try {
//            jsonBody.put("user_id", Tools.getData(context, "idprofile"));
//            jsonBody.put("pub_id", data.get(position).getId());
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        JsonObjectRequest reqDelete = new JsonObjectRequest(context.getString(R.string.server_url) + context.getString(R.string.delete_pub), jsonBody,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//                            if (response.getBoolean("result")) {
//
//                                data.remove(position);
//                                notifyDataSetChanged();
//                            }
//
//                        } catch (JSONException e) {
//                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                //if (context != null)
//                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("lang", Tools.getData(context, "lang_pr"));
//                headers.put("token", Tools.getData(context, "token"));
//                return headers;
//            }
//        };
//
//        ApplicationController.getInstance().addToRequestQueue(reqDelete);
//    }
//
//
//
//    @Override
//    public int getItemViewType(int position) {
//        return position;
//    }
//
//    private static class ViewHolder {
//        TextView tvTitle, tvStatus, tvDate,tvUserDesc;
//        LinearLayout llContent;
//        ImageView ivPhoto;
//        Button trashBtn;
//
//        public ViewHolder(View view) {
//            tvTitle = (TextView) view.findViewById(R.id.tvTitle);
//            tvStatus = (TextView) view.findViewById(R.id.tvStatus);
//            tvDate = (TextView) view.findViewById(R.id.tvDate);
//            llContent = (LinearLayout) view.findViewById(R.id.llContent);
//            ivPhoto = (ImageView) view.findViewById(R.id.ivPhoto);
//            tvUserDesc=(TextView) view.findViewById(R.id.tvUserDesc);
////            trashBtn=(Button)view.findViewById(R.id.trashBtn);
//
//            view.setTag(this);
//        }
//    }
//}










package com.oec.wis.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.entities.WISAds;
import com.oec.wis.tools.Tools;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class MyAdsAdapter extends BaseAdapter {
    Context context;
    List<WISAds> data;

    public MyAdsAdapter(Context context, List<WISAds> data) {
        this.data = data;
        this.context = context;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return data.get(position).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.row_my_ads, null);
            holder = new ViewHolder(convertView);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.ivPhoto.setImageResource(0);


        holder.tvTitle.setText(data.get(position).getTitle());

        System.out.println("data values" + data.get(position).toString());
        System.out.println("description" + data.get(position).getDesc());


//        Created date2017-02-02 09:32:15


        String cDate = data.get(position).getcDate();
        String lDate = data.get(position).getlDate();

        System.out.println("Created date" + cDate + "last date" + lDate);

        String desc = data.get(position).getDesc();
        System.out.println("description" + desc);

//        holder.tvUserDesc.setText(data.get(position).getDesc());
//
//
//        try{
//            String desc=data.get(position).getDesc();
//            Log.d("desc",desc);
//            Log.d("data",data.toString());
//
//            holder.tvUserDesc.setText(data.get(position).getDesc());
//        }catch (NullPointerException ex){
//            System.out.println("ex"+ex.getMessage());
//        }


        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/Harmattan-Regular.ttf");
        holder.tvTitle.setTypeface(font);
        holder.tvDate.setTypeface(font);
        holder.tvUserDesc.setTypeface(font);
        holder.tvStatus.setTypeface(font);
        holder.atTimeTV.setTypeface(font);


//        if (data.get(position).getStatus() == 1) {
//            holder.tvStatus.setText(context.getString(R.string.progress));
////            holder.llContent.setBackgroundResource(R.drawable.llayout3);
//        } else {
//            holder.tvStatus.setText(context.getString(R.string.expired));
////            holder.llContent.setBackgroundResource(R.drawable.llayout5);
//        }
//        holder.tvStatus.setText("31/1/2017");

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat format_two = new SimpleDateFormat("dd/MM/yy");
        SimpleDateFormat format_three = new SimpleDateFormat("dd/MMMM/yyyy");
        SimpleDateFormat hour = new SimpleDateFormat("H");
        SimpleDateFormat min = new SimpleDateFormat("mm");
        SimpleDateFormat sec = new SimpleDateFormat("ss");
        Date d = null;
        String h = "", m = "", s = "";
        String created_at = null;
        try {
            d = format.parse(data.get(position).getcDate());
            created_at = format_two.format(d);
            h = hour.format(d);
            m = min.format(d);
            s = sec.format(d);

            String text = h.concat("H").concat(m).concat("'").concat(s);
            String datetext = h.concat(":").concat(m);
            System.out.println("date====>" + datetext + "text" + text);
//            holder.tvDate.setText(datetext);

            holder.trashBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteDialog(position);
                }
            });
            // Get date from string
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = dateFormatter.parse(data.get(position).getcDate());
//
//// Get time from date
            SimpleDateFormat timeFormatter = new SimpleDateFormat("h:mm a");

            String displayValue = timeFormatter.format(date);
            String timeStr = displayValue.toLowerCase(Locale.UK);/*small letere*/
//            String exactvalue = displayValue.replace("AM", "am").replace("PM","pm");

            System.out.println("display value" + displayValue);
            System.out.println("display exactvalue" + timeStr);
            holder.tvDate.setText(timeStr);

        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.tvStatus.setText(created_at);
        if (data.get(position).getTypeObj().equals("video")) {
            ApplicationController.getInstance().getImageLoader().get(context.getString(R.string.server_url4) + data.get(position).getThumb(), new ImageLoader.ImageListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    holder.ivPhoto.setImageResource(R.drawable.empty);
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        holder.ivPhoto.setImageBitmap(response.getBitmap());
                    } else {
                        holder.ivPhoto.setImageResource(R.drawable.empty);
                    }
                }
            });
        } else {
            ApplicationController.getInstance().getImageLoader().get(context.getString(R.string.server_url3) + data.get(position).getPhoto(), new ImageLoader.ImageListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    holder.ivPhoto.setImageResource(R.drawable.empty);
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        holder.ivPhoto.setImageBitmap(response.getBitmap());
                    } else {
                        holder.ivPhoto.setImageResource(R.drawable.empty);
                    }
                }
            });
        }
//        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        Date d = null;
//        try {
//            d = format.parse(data.get(position).getcDate());
//            Date date =format.parse(data.get(position).getEndTime());
//
//            Log.d("date 1111" , String.valueOf(date));
//
//            Log.d("date" , String.valueOf(d));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
////        String rDate = net.danlew.android.joda.DateUtils.getRelativeTimeSpanString(context, new DateTime(d), true).toString();
//        String rDate = net.danlew.android.joda.DateUtils.getRelativeTimeSpanString(context, new DateTime(d), true).toString();
//
//        holder.tvDate.setText(rDate);

        return convertView;
    }

    private void deleteDialog(final int position) {
        new AlertDialog.Builder(context)
                .setMessage(context.getString(R.string.msg_confirm_delete_post))
                .setPositiveButton(context.getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        removePubs(position);
                    }

                })
                .setNegativeButton(context.getString(R.string.no), null)
                .show();
    }

    private void removePubs(final int position) {
        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("user_id", Tools.getData(context, "idprofile"));
            jsonBody.put("pub_id", data.get(position).getId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqDelete = new JsonObjectRequest(context.getString(R.string.server_url) + context.getString(R.string.delete_pub), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {

                                data.remove(position);
                                notifyDataSetChanged();
                            }

                        } catch (JSONException e) {
                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //if (context != null)
                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("lang", Tools.getData(context, "lang_pr"));
                headers.put("token", Tools.getData(context, "token"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqDelete);
    }



    private static class ViewHolder {
        TextView tvTitle, tvStatus, tvDate,tvUserDesc,atTimeTV;
        LinearLayout llContent;
        ImageView ivPhoto;
        ImageView trashBtn;

        public ViewHolder(View view) {
            tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            tvStatus = (TextView) view.findViewById(R.id.tvStatus);
            tvDate = (TextView) view.findViewById(R.id.tvDate);
            llContent = (LinearLayout) view.findViewById(R.id.llContent);
            ivPhoto = (ImageView) view.findViewById(R.id.ivPhoto);
            tvUserDesc=(TextView) view.findViewById(R.id.tvUserDesc);
            trashBtn=(ImageView) view.findViewById(R.id.trashBtn);
            atTimeTV=(TextView)view.findViewById(R.id.atTimeTV);
            view.setTag(this);
        }
    }


    @Override
    public int getViewTypeCount() {
        if (data.size() > 1)
            return data.size();
        else
            return 1;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


}
