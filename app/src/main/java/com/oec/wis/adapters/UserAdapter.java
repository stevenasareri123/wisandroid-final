package com.oec.wis.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.github.siyamed.shapeimageview.RoundedImageView;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.entities.WISUser;

import java.util.List;

/**
 * Created by asareri08 on 16/08/16.
 */
public class UserAdapter extends BaseAdapter {

    List<WISUser> data;
    Context context;

    public UserAdapter(Context context, List<WISUser> data){

        this.context = context;
        this.data = data;


    }
    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return data.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.user_row_item, null);
            holder = new ViewHolder(convertView);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.ivPhoto.setImageResource(0);
        Log.i("name",data.get(position).getFirstName());
        holder.tvName.setText(data.get(position).getFirstName());
        ApplicationController.getInstance().getImageLoader().get(context.getString(R.string.server_url3) + data.get(position).getPic(), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                holder.ivPhoto.setImageResource(R.drawable.profile);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    holder.ivPhoto.setImageBitmap(response.getBitmap());
                } else {
                    holder.ivPhoto.setImageResource(R.drawable.profile);
                }
            }
        });

//        holder.ivPhoto.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent i = new Intent(context, ProfileView.class);
//                i.putExtra("id", data.get(position).getId());
//                context.startActivity(i);
//            }
//        });
        return convertView;

    }

    private static class ViewHolder {
        TextView tvName, tvCFriend;
        RoundedImageView ivPhoto;
        Button bInvit;
        ProgressBar progress;

        public ViewHolder(View view) {
            tvName = (TextView) view.findViewById(R.id.name);
            ivPhoto = (RoundedImageView) view.findViewById(R.id.userImage);
            view.setTag(this);
        }
    }


}
