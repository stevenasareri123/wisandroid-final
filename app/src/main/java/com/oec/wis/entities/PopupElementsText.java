package com.oec.wis.entities;

/**
 * Created by asareri-07 on 2/15/2017.
 */

public class PopupElementsText {
    public String getTitle() {
        return titlename;
    }

    public void setTitle(String titlename) {
        this.titlename = titlename;
    }

    public String titlename;

    public PopupElementsText(String titlename) {
        this.titlename = titlename;
    }


}
