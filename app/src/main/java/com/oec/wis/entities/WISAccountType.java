package com.oec.wis.entities;

public enum WISAccountType {
    Business,
    Particular,
    Associations,
    Organizations,
    Other
}
