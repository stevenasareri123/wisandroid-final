package com.oec.wis.entities;

import java.util.Date;

/**
 * Created by asareri08 on 21/07/16.
 */
public class Event{
    private Date date;
    private int color;

    public Event(Date date, int color) {
        this.date = date;
        this.color = color;
    }

    public Date getDate() {
        return date;
    }

    public int getColor() {
        return color;
    }
}