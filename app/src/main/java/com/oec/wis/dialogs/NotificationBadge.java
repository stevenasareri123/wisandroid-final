package com.oec.wis.dialogs;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.oec.wis.R;

import me.leolin.shortcutbadger.ShortcutBadger;

public class NotificationBadge extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_badge);
    }

    public void showBadge(){
        Log.i("show badge","bagde");


        int badgeCount = 0;
        try {
            badgeCount = 2;
        } catch (NumberFormatException e) {


            Log.i("Error",e.getLocalizedMessage());
        }
        ShortcutBadger.applyCount(NotificationBadge.this, badgeCount);
//        boolean success = ShortcutBadger.applyCount(NotificationBadge.this, badgeCount);

//        Toast.makeText(getApplicationContext(), "Set count=" + badgeCount + ", success=" + success, Toast.LENGTH_SHORT).show();

        //find the home launcher Package
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        ResolveInfo resolveInfo = getPackageManager().resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY);
        String currentHomePackage = resolveInfo.activityInfo.packageName;

        TextView textViewHomePackage = (TextView) findViewById(R.id.textViewHomePackage);
        textViewHomePackage.setText("launcher:" + currentHomePackage);

    }
}
