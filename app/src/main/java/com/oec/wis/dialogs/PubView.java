package com.oec.wis.dialogs;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.entities.WISPhoto;
import com.oec.wis.fragments.FragPhoto;
import com.oec.wis.tools.Tools;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PubView extends AppCompatActivity {
    ImageView ivPhoto;
    TextView tvTitle, tvDesc;
    String video = "";
    ImageView bChat;
    int idAnnouncer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pub_view);
        loadControls();
        loadData();
        setListener();
    }

    private void loadControls() {
        ivPhoto = (ImageView) findViewById(R.id.ivPhoto);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvDesc = (TextView) findViewById(R.id.tvDesc);
        bChat = (ImageView) findViewById(R.id.bChat);
    }

    private void setListener() {
        findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        findViewById(R.id.ivVideo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse(getString(R.string.server_url3) + video);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                intent.setDataAndType(uri, "video/mp4");
                startActivity(intent);
            }
        });
        bChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(PubView.this, Chat.class);
                i.putExtra("id", idAnnouncer);
                startActivity(i);
            }
        });
    }



    private void loadData() {
        findViewById(R.id.loading).setVisibility(View.VISIBLE);
        findViewById(R.id.scrollView2).setVisibility(View.INVISIBLE);
        JSONObject jsonBody = new JSONObject();
        int id = getIntent().getExtras().getInt("idpub");
        try {
            jsonBody.put("id_profil", Tools.getData(this, "idprofile"));
            jsonBody.put("id_pub", id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqPub = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.getpub_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {


                                Log.i("pub list", String.valueOf(response));

                                final JSONObject obj = response.getJSONObject("data");
                                idAnnouncer = Integer.parseInt(obj.getString("created_by"));
                                if (obj.getString("titre") != null)
                                    tvTitle.setText(obj.getString("titre"));
                                if (obj.getString("description") != null)
                                    tvDesc.setText(obj.getString("description"));
                                if (obj.getString("type_obj").equals("photo")) {
                                    final String photo = obj.getString("photo");
                                    final String date = obj.getString("created_at");
                                    ApplicationController.getInstance().getImageLoader().get(getString(R.string.server_url3) + photo, new ImageLoader.ImageListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            ivPhoto.setImageResource(R.drawable.empty);
                                        }

                                        @Override
                                        public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                                            if (response.getBitmap() != null) {
                                                ivPhoto.setImageBitmap(response.getBitmap());
                                            } else {
                                                ivPhoto.setImageResource(R.drawable.empty);
                                            }
                                        }
                                    });
                                    findViewById(R.id.ivVideo).setVisibility(View.INVISIBLE);
                                    ivPhoto.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            FragPhoto.photos = new ArrayList<>();
                                            FragPhoto.photos.add(new WISPhoto(0, "", photo, date));
                                            Intent i = new Intent(PubView.this, PhotoGallery.class);
                                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            i.putExtra("p", 0);
                                            startActivity(i);
                                        }
                                    });
                                } else if (obj.getString("type_obj").equals("video")) {
                                    findViewById(R.id.ivVideo).setVisibility(View.VISIBLE);
                                    ApplicationController.getInstance().getImageLoader().get(getString(R.string.server_url4) + obj.getString("photo_video"), new ImageLoader.ImageListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            ivPhoto.setImageResource(R.drawable.empty);
                                        }

                                        @Override
                                        public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                                            if (response.getBitmap() != null) {
                                                ivPhoto.setImageBitmap(response.getBitmap());
                                            } else {
                                                ivPhoto.setImageResource(R.drawable.empty);
                                            }
                                        }
                                    });
                                }

                                findViewById(R.id.scrollView2).setVisibility(View.VISIBLE);
                            }
                            findViewById(R.id.loading).setVisibility(View.GONE);

                        } catch (JSONException e) {
                            findViewById(R.id.loading).setVisibility(View.GONE);
                            //Toast.makeText(PubView.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(PubView.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(PubView.this, "token"));
                headers.put("lang", Tools.getData(PubView.this, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqPub);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ApplicationController.getInstance().cancelPendingRequests(ApplicationController.TAG);
    }
}
