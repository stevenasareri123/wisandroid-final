package com.oec.wis.wisdirect;

/**
 * Created by asareri12 on 15/03/17.
 */

public interface ControlListener {
    public void onControlsClicked();
}
