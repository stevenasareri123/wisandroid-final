package com.oec.wis;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.oec.wis.dialogs.DemoSample;
import com.oec.wis.tools.Tools;

import java.io.IOException;

public class SplashSreen extends Activity {
    GoogleCloudMessaging gcm = null;
    String regId = "";
    private int TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_sreen);
        getGCMId();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                if (TextUtils.isEmpty(Tools.getData(SplashSreen.this, "token"))) {
                    Intent i = new Intent(SplashSreen.this, Login.class);
                    startActivity(i);
                } else {
                    startActivity(new Intent(getApplicationContext(), Dashboard.class));



                }

                finish();
            }
        }, TIME_OUT);
    }

    private void getGCMId() {

        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging
                                .getInstance(getApplicationContext());
                    }
                    regId = gcm.register(getString(R.string.gcm_project));

                } catch (IOException ex) {
                }
                return "";
            }

            @Override
            protected void onPostExecute(String msg) {
                if (regId != null) {
                    Tools.saveData(SplashSreen.this, "regid", regId);
                } else {
                    Toast.makeText(SplashSreen.this, SplashSreen.this.getString(R.string.msg_gplays), Toast.LENGTH_LONG).show();
                }
            }
        }.execute(null, null, null);
    }

    @Override
    public void onBackPressed(){



        moveTaskToBack(true);

    }
}
