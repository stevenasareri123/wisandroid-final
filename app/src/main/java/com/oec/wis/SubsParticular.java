package com.oec.wis;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.oec.wis.adapters.CountryAdapter;
import com.oec.wis.entities.WISCountry;
import com.oec.wis.tools.CustomPhoneNumberFormattingTextWatcher;
import com.oec.wis.tools.OnPhoneChangedListener;
import com.oec.wis.tools.PhoneUtils;
import com.oec.wis.tools.Tools;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

public class SubsParticular extends AppCompatActivity {
    protected static final TreeSet<String> CANADA_CODES = new TreeSet<>();

    static {
        CANADA_CODES.add("204");
        CANADA_CODES.add("236");
        CANADA_CODES.add("249");
        CANADA_CODES.add("250");
        CANADA_CODES.add("289");
        CANADA_CODES.add("306");
        CANADA_CODES.add("343");
        CANADA_CODES.add("365");
        CANADA_CODES.add("387");
        CANADA_CODES.add("403");
        CANADA_CODES.add("416");
        CANADA_CODES.add("418");
        CANADA_CODES.add("431");
        CANADA_CODES.add("437");
        CANADA_CODES.add("438");
        CANADA_CODES.add("450");
        CANADA_CODES.add("506");
        CANADA_CODES.add("514");
        CANADA_CODES.add("519");
        CANADA_CODES.add("548");
        CANADA_CODES.add("579");
        CANADA_CODES.add("581");
        CANADA_CODES.add("587");
        CANADA_CODES.add("604");
        CANADA_CODES.add("613");
        CANADA_CODES.add("639");
        CANADA_CODES.add("647");
        CANADA_CODES.add("672");
        CANADA_CODES.add("705");
        CANADA_CODES.add("709");
        CANADA_CODES.add("742");
        CANADA_CODES.add("778");
        CANADA_CODES.add("780");
        CANADA_CODES.add("782");
        CANADA_CODES.add("807");
        CANADA_CODES.add("819");
        CANADA_CODES.add("825");
        CANADA_CODES.add("867");
        CANADA_CODES.add("873");
        CANADA_CODES.add("902");
        CANADA_CODES.add("905");
    }

    protected PhoneNumberUtil mPhoneNumberUtil = PhoneNumberUtil.getInstance();
    protected SparseArray<ArrayList<WISCountry>> mCountriesMap = new SparseArray<>();
    protected String mLastEnteredPhone;
    protected AdapterView.OnItemSelectedListener mOnItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            WISCountry c = (WISCountry) spCountry.getItemAtPosition(position);
            if (mLastEnteredPhone != null && mLastEnteredPhone.startsWith(c.getCountryCodeStr())) {
                return;
            }
            etPhone.getText().clear();
            etPhone.getText().insert(etPhone.getText().length() > 0 ? 1 : 0, String.valueOf(c.getCountryCode()));
            etPhone.setSelection(etPhone.length());
            mLastEnteredPhone = null;
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };
    protected OnPhoneChangedListener mOnPhoneChangedListener = new OnPhoneChangedListener() {
        @Override
        public void onPhoneChanged(String phone) {
            try {
                mLastEnteredPhone = phone;
                Phonenumber.PhoneNumber p = mPhoneNumberUtil.parse(phone, null);
                ArrayList<WISCountry> list = mCountriesMap.get(p.getCountryCode());
                WISCountry country = null;
                if (list != null) {
                    if (p.getCountryCode() == 1) {
                        String num = String.valueOf(p.getNationalNumber());
                        if (num.length() >= 3) {
                            String code = num.substring(0, 3);
                            if (CANADA_CODES.contains(code)) {
                                for (WISCountry c : list) {
                                    // Canada has priority 1, US has priority 0
                                    if (c.getPriority() == 1) {
                                        country = c;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    if (country == null) {
                        for (WISCountry c : list) {
                            if (c.getPriority() == 0) {
                                country = c;
                                break;
                            }
                        }
                    }
                }
                if (country != null) {
                    final int position = country.getNum();
                    spCountry.post(new Runnable() {
                        @Override
                        public void run() {
                            spCountry.setSelection(position);
                        }
                    });
                }
            } catch (NumberParseException ignore) {
            }

        }
    };
    Spinner spCountries, spTimezone, spCountry;
    EditText etName, etFName, etState, etPhone;
    CountryAdapter cAdapter;
    String name, email, pwd, lang;
    String photoPath;
    DatePicker dpBirthday;
    CheckBox cvAgree;
    RadioButton rMan, rWMan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Tools.setLocale(this, getIntent().getExtras().getString("lang").substring(0, getIntent().getExtras().getString("lang").indexOf("_")));
        setContentView(R.layout.activity_subs_particular);
        loadControls();
        setListener();
        loadSpinnerData();
        loadExtras();
    }

    private void loadExtras() {
        name = getIntent().getExtras().getString("name");
        email = getIntent().getExtras().getString("email");
        pwd = getIntent().getExtras().getString("pwd");
        etName.setText(name);
        lang = getIntent().getExtras().getString("lang");
        photoPath = getIntent().getExtras().getString("photo");
    }

    @Override
    protected void onStart() {
        super.onStart();
        initCodes(this);
    }

    private void setListener() {
        spCountry.setOnItemSelectedListener(mOnItemSelectedListener);
        etPhone.addTextChangedListener(new CustomPhoneNumberFormattingTextWatcher(mOnPhoneChangedListener));
        InputFilter filter = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {
                    char c = source.charAt(i);
                    if (dstart > 0 && !Character.isDigit(c)) {
                        return "";
                    }
                }
                return null;
            }
        };
        etPhone.setFilters(new InputFilter[]{filter});
        findViewById(R.id.bSubs).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check()) {
                    doSubs();
                }
            }
        });
        spTimezone.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                spTimezone.setSelection(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void loadControls() {
        spCountry = (Spinner) findViewById(R.id.cFlag);
        spTimezone = (Spinner) findViewById(R.id.spTimezone);
        etPhone = (EditText) findViewById(R.id.etPhone);
        spCountries = (Spinner) findViewById(R.id.spCountry);
        etName = (EditText) findViewById(R.id.etLastName);
        etFName = (EditText) findViewById(R.id.etFirstName);
        etState = (EditText) findViewById(R.id.etState);
        dpBirthday = (DatePicker) findViewById(R.id.dpBirthday);
        cvAgree = (CheckBox) findViewById(R.id.cbAgree);
        rMan = (RadioButton) findViewById(R.id.rMale);
        rWMan = (RadioButton) findViewById(R.id.rFemal);
    }

    private void loadSpinnerData() {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.activities));
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        dataAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.lang_list));
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cAdapter = new CountryAdapter(this);
        spCountry.setAdapter(cAdapter);
        dataAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.count_list));
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCountries.setAdapter(dataAdapter);
        dataAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, getTimezone());
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTimezone.setAdapter(dataAdapter);
    }
    private ArrayList  getTimezone(){
        ArrayList timeZoneList=new ArrayList<>();
        String[] ids = TimeZone.getAvailableIDs();
        for (String id : ids) {
            timeZoneList.add(displayTimeZone(TimeZone.getTimeZone(id)));
        }
        return timeZoneList;
    }

    private static String displayTimeZone(TimeZone tz) {

        long hours = TimeUnit.MILLISECONDS.toHours(tz.getRawOffset());
        long minutes = TimeUnit.MILLISECONDS.toMinutes(tz.getRawOffset())
                - TimeUnit.HOURS.toMinutes(hours);
        // avoid -4:-30 issue
        minutes = Math.abs(minutes);

        String result = "";
        if (hours > 0) {
            result = String.format("(GMT+%d:%02d) %s", hours, minutes, tz.getID());
        } else {
            result = String.format("(GMT%d:%02d) %s", hours, minutes, tz.getID());
        }

        return result;

    }

    protected void initCodes(Context context) {
        new AsyncPhoneInitTask(context).execute();
    }

    protected String validatePhone() {
        String region = null;
        String phone = null;
        if (mLastEnteredPhone != null) {
            try {
                Phonenumber.PhoneNumber p = mPhoneNumberUtil.parse(mLastEnteredPhone, null);
                StringBuilder sb = new StringBuilder(16);
                sb.append('+').append(p.getCountryCode()).append(p.getNationalNumber());
                phone = sb.toString();
                region = mPhoneNumberUtil.getRegionCodeForNumber(p);
            } catch (NumberParseException ignore) {
            }
        }
        if (region != null) {
            return phone;
        } else {
            return null;
        }
    }

    private boolean check() {
        boolean check = true;
        if (TextUtils.isEmpty(etName.getText().toString())) {
            check = false;
            etName.setError(getString(R.string.msg_empty_name));
        }

        if (TextUtils.isEmpty(etFName.getText().toString())) {
            check = false;
            etFName.setError(getString(R.string.msg_empty_fname));
        }

        if (TextUtils.isEmpty(etState.getText().toString())) {
            check = false;
            etState.setError(getString(R.string.msg_empty_state));
        }
        etPhone.setError(null);
        String phone = validatePhone();
        if (phone == null) {
            check = false;
            etPhone.setError(getString(R.string.msg_invalid_phone));
        }
        if (!cvAgree.isChecked() && check) {
            check = false;
            Toast.makeText(getApplicationContext(), getString(R.string.msg_not_agree), Toast.LENGTH_SHORT).show();
        }
        return check;
    }

    private void doSubs() {
        findViewById(R.id.loading).setVisibility(View.VISIBLE);
        JSONObject jsonBody = new JSONObject();
/*{
    "language": "fr_FR",
    "email": "test@testqsdqdss.com",
    "tel": "000 00 000 000",
    "password": "root",
    "typeaccount": "Other",
    "photo": "img.jpg",
    "name_representant_etr": "Mr X",
    "name": "Open eyes",
    "activite": "Dev && Consulting",
    "date_birth": "1987-01-01",
    "place_addre": "Tunis",
    "special_other": "XXX",
    "code_ape_etr": "DSY565SQ",
    "firstname_prt": "Jhon",
    "lastname_prt": "Thomas",
    "sexe_prt": "Homme",
    "id_gcm": "SDSDS6550"
    Business
Particular
Associations
Organizations
Other
}*/
        try {
            jsonBody.put("language", lang);
            jsonBody.put("email", email);
            jsonBody.put("tel", etPhone.getText().toString());
            jsonBody.put("password", pwd);
            jsonBody.put("typeaccount", "Particular");
            jsonBody.put("photo", "");
            jsonBody.put("name_representant_etr", "");
            jsonBody.put("name", etName.getText().toString());
            jsonBody.put("activite", "");
            int day = dpBirthday.getDayOfMonth();
            int month = dpBirthday.getMonth();
            int year = dpBirthday.getYear();

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String formatedDate = sdf.format(new Date(year - 1900, month, day));
            jsonBody.put("date_birth", formatedDate);
            jsonBody.put("place_addre", "");
            jsonBody.put("special_other", "");
            jsonBody.put("code_ape_etr", "");
            jsonBody.put("firstname_prt", etFName.getText().toString());
            jsonBody.put("lastname_prt", etName.getText().toString());
            if (rMan.isChecked())
                jsonBody.put("sexe_prt", "Homme");
            else
                jsonBody.put("sexe_prt", "Femme");
            jsonBody.put("id_gcm", Tools.getData(this, "regid"));
            jsonBody.put("state", etState.getText().toString());
            jsonBody.put("country", getResources().getStringArray(R.array.count_list)[spCountries.getSelectedItemPosition()]);

            Object[] spTimezoneArray = getTimezone().toArray();
            String str = spTimezoneArray[spTimezone.getSelectedItemPosition()].toString();
            String answer = str.substring(str.indexOf("(")+1,str.indexOf(")"));
            String[] separated = str.split("");

            String CurrentString = spTimezone.getSelectedItem().toString();
            String[] timezone_values = CurrentString.split(" ");


            jsonBody.put("time_zone",timezone_values[1]);
            jsonBody.put("time_zone_format", timezone_values[0]);



        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqSubs = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.subs_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                JSONObject object = response.getJSONObject("data");
                                saveUData(object.getString("token"), object.getString("id_profil"));
                                if (!TextUtils.isEmpty(photoPath)) {
                                    new DoUpload().execute();
                                } else {
                                    Toast.makeText(getApplicationContext(), getString(R.string.msg_subs_succes), Toast.LENGTH_SHORT).show();
                                    finish();
                                    Intent intent = new Intent(SubsParticular.this, Dashboard.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    SubsParticular.this.startActivity(intent);
                                }
                            } else {
                                if (response.getString("Message").contains("Email already exists"))
                                    Toast.makeText(getApplicationContext(), getString(R.string.err_email_already), Toast.LENGTH_SHORT).show();
                                findViewById(R.id.loading).setVisibility(View.GONE);
                            }

                        } catch (JSONException e) {
                            findViewById(R.id.loading).setVisibility(View.GONE);
                            //Toast.makeText(getApplicationContext(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(getApplicationContext(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        });
        ApplicationController.getInstance().addToRequestQueue(reqSubs);
    }

    private void saveUData(String token, String id_profile) throws JSONException {
        Tools.saveData(getApplicationContext(), "firstname_prt", etName.getText().toString());
        Tools.saveData(getApplicationContext(), "lastname_prt", etFName.getText().toString());
        Tools.saveData(getApplicationContext(), "token", token);
        Tools.saveData(getApplicationContext(), "photo", "");
        Tools.saveData(getApplicationContext(), "last_connected", "");
        Tools.saveData(getApplicationContext(), "idprofile", id_profile);
        if (rMan.isChecked())
            Tools.saveData(getApplicationContext(), "sexe_prt", "Homme");
        else
            Tools.saveData(getApplicationContext(), "sexe_prt", "Femme");
        Tools.saveData(getApplicationContext(), "lang_pr", lang.substring(0, lang.indexOf("_")));
        Tools.saveData(getApplicationContext(), "tel_pr", etPhone.getText().toString());
        Tools.saveData(getApplicationContext(), "profiletype", "Particular");
        Tools.saveData(getApplicationContext(), "code_ape_etr", "");
        Tools.saveData(getApplicationContext(), "email_pr", email);
        Tools.saveData(getApplicationContext(), "special_other", "");
        Tools.saveData(getApplicationContext(), "place_addre", "");
        Tools.saveData(getApplicationContext(), "activite", "");
        Tools.saveData(getApplicationContext(), "registered", "");
        Tools.saveData(getApplicationContext(), "state", etState.getText().toString());
        Tools.saveData(getApplicationContext(), "country", getResources().getStringArray(R.array.count_list)[spCountries.getSelectedItemPosition()]);
        int day = dpBirthday.getDayOfMonth();
        int month = dpBirthday.getMonth();
        int year = dpBirthday.getYear();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String formatedDate = sdf.format(new Date(year - 1900, month, day));
        Tools.saveData(getApplicationContext(), "date_birth", formatedDate);
        Tools.saveData(getApplicationContext(), "name_representant_etr", "");
        Tools.saveData(getApplicationContext(), "pwd", pwd);

        String CurrentString = spTimezone.getSelectedItem().toString();
        String[] timezone_values = CurrentString.split(" ");

        Tools.saveData(getApplicationContext(),"timezone",timezone_values[1]);
        Tools.saveData(getApplicationContext(),"timezone_format",timezone_values[0]);
    }

    private void doUpdatePhoto(String photo) {
        JSONObject jsonBody = null;

        try {
            String json = "{\"id_profil\": \"" + Tools.getData(this, "idprofile") + "\",\"name_img\": \"" + photo + "\"}";
            jsonBody = new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqUpdate = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.update_img_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                Toast.makeText(getApplicationContext(), getString(R.string.msg_subs_succes), Toast.LENGTH_SHORT).show();
                                finish();
                                Intent intent = new Intent(SubsParticular.this, Dashboard.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                SubsParticular.this.startActivity(intent);
                            }
                        } catch (Exception e) {
                            Toast.makeText(getApplicationContext(), getString(R.string.msg_subs_succes), Toast.LENGTH_SHORT).show();
                            finish();
                            Intent intent = new Intent(SubsParticular.this, Dashboard.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            SubsParticular.this.startActivity(intent);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), getString(R.string.msg_subs_succes), Toast.LENGTH_SHORT).show();
                finish();
                Intent intent = new Intent(SubsParticular.this, Dashboard.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                SubsParticular.this.startActivity(intent);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(SubsParticular.this, "token"));
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("lang", "en_US");
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqUpdate);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ApplicationController.getInstance().cancelPendingRequests(ApplicationController.TAG);
    }

    protected class AsyncPhoneInitTask extends AsyncTask<Void, Void, ArrayList<WISCountry>> {

        private int mSpinnerPosition = -1;
        private Context mContext;

        public AsyncPhoneInitTask(Context context) {
            mContext = context;
        }

        @Override
        protected ArrayList<WISCountry> doInBackground(Void... params) {
            ArrayList<WISCountry> data = new ArrayList<WISCountry>(233);
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new InputStreamReader(mContext.getApplicationContext().getAssets().open("countries.dat"), "UTF-8"));

                String line;
                int i = 0;
                while ((line = reader.readLine()) != null) {
                    WISCountry c = new WISCountry(mContext, line, i);
                    data.add(c);
                    ArrayList<WISCountry> list = mCountriesMap.get(c.getCountryCode());
                    if (list == null) {
                        list = new ArrayList<WISCountry>();
                        mCountriesMap.put(c.getCountryCode(), list);
                    }
                    list.add(c);
                    i++;
                }
            } catch (IOException e) {

            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        //log the exception
                    }
                }
            }
            if (!TextUtils.isEmpty(etPhone.getText())) {
                return data;
            }
            String countryRegion = PhoneUtils.getCountryRegionFromPhone(mContext);
            int code = mPhoneNumberUtil.getCountryCodeForRegion(countryRegion);
            ArrayList<WISCountry> list = mCountriesMap.get(code);
            if (list != null) {
                for (WISCountry c : list) {
                    if (c.getPriority() == 0) {
                        mSpinnerPosition = c.getNum();
                        break;
                    }
                }
            }
            return data;
        }

        @Override
        protected void onPostExecute(ArrayList<WISCountry> data) {
            cAdapter.addAll(data);
            if (mSpinnerPosition > 0) {
                spCountry.setSelection(mSpinnerPosition);
            }
        }
    }

    private class DoUpload extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            findViewById(R.id.loading).setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (!result.equals("")) {
                try {
                    JSONObject img = new JSONObject(result);
                    if (img.getBoolean("result")) {
                        Tools.saveData(getApplicationContext(), "photo", img.getString("data"));
                        doUpdatePhoto(img.getString("data"));
                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.msg_subs_succes), Toast.LENGTH_SHORT).show();
                        finish();
                        Intent intent = new Intent(SubsParticular.this, Dashboard.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        SubsParticular.this.startActivity(intent);
                    }
                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(), getString(R.string.msg_subs_succes), Toast.LENGTH_SHORT).show();
                    finish();
                    Intent intent = new Intent(SubsParticular.this, Dashboard.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    SubsParticular.this.startActivity(intent);
                }

            } else {
                Toast.makeText(getApplicationContext(), getString(R.string.msg_subs_succes), Toast.LENGTH_SHORT).show();
                finish();
                Intent intent = new Intent(SubsParticular.this, Dashboard.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                SubsParticular.this.startActivity(intent);
            }
        }

        @Override
        protected String doInBackground(String... urls) {
            return Tools.doFileUpload(SubsParticular.this.getString(R.string.server_url) + SubsParticular.this.getString(R.string.upload_profile_meth), photoPath, Tools.getData(SubsParticular.this, "token"));
        }
    }
}
