package com.oec.wis.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.location.Location;
import android.app.Fragment;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.oec.wis.ApplicationController;
import com.oec.wis.Dashboard;
import com.oec.wis.R;
import com.oec.wis.adapters.Popupadapter;
import com.oec.wis.adapters.SelectFriendsAdapter;
import com.oec.wis.adapters.UserLocationListener;
import com.oec.wis.dialogs.MapsActivity;
import com.oec.wis.dialogs.ProfileView;
import com.oec.wis.dialogs.ProfileViewTwo;
import com.oec.wis.dialogs.PubDetails;
import com.oec.wis.entities.EntryItem;
import com.oec.wis.entities.Popupelements;
import com.oec.wis.entities.SectionItem;
import com.oec.wis.entities.UserNotification;
import com.oec.wis.entities.WISUser;
import com.oec.wis.tools.Tools;
import com.oec.wis.tools.UserLocation;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnItemClickListener;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;


///**
// * A simple {@link Fragment} subclass.
// * Activities that contain this fragment must implement the
// * {@link BlankFragment.OnFragmentInteractionListener} interface
// * to handle interaction events.
// * Use the {@link BlankFragment#newInstance} factory method to
// * create an instance of this fragment.
// */
public class BlankFragment extends Fragment implements OnMapReadyCallback {
    private GoogleMap googleMap;

    List<WISUser> friendList;

    ArrayList items;
    LatLng myLocation = null;
    String address;
    View view;
    ImageView toggle;
    ListView groupChatFriendList;

    EditText searchFilter;
    Popupadapter simpleAdapter;

    SelectFriendsAdapter selectFriendsAdapter;


    String snapShotFilePath;

    double latitude, langitude;

    String currentLocationName;


    String curretnLocationAdress;

    String coordinates;
    PopupWindow popWindow;
    Button addMapOption;

    ImageView close_btn;
    Button post_status;
    ImageView snapShot;
    TextView message;

    SupportMapFragment mapFragment;
    ArrayList popupList;public static final String PREFS_NAME = "CustomPermission";
    SharedPreferences settings;
    ProgressBar loading;
    DrawerLayout drawer_layout;
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
//        }
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_blank, container, false);
//
//
//        setListener();

        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            Tools.setLocale(getActivity(), Tools.getData(getActivity(), "lang_pr"));
            view = inflater.inflate(R.layout.fragment_blank, container, false);
            setupMapView();
            setListener();





        } catch (InflateException e) {
        }


//        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
//        SupportMapFragment mapFragment = (SupportMapFragment) getActivity().getSupportFragmentManager()
//                .findFragmentById(R.id.map);
//        mapFragment.getMapAsync(this);

        Boolean locationEnabled = Tools.showGPSDialog(getActivity());

        if (locationEnabled) {
            settings = getActivity().getSharedPreferences(PREFS_NAME, 0);
            boolean dialogShown = settings.getBoolean("locationPermissionDialogShown", false);

            if (!dialogShown) {
                // AlertDialog code here
                showMapPermissionAlert();

            } else {
                loadFriends();

//                Log.i("CurrentLoc",curretnLocationAdress);
//                Log.i("CurrentLoc",coordinates);


            }
        }
        return view;
    }


    private void setupMapView() {
        try{
            loadFriends();


            addMapOption = (Button) view.findViewById(R.id.addMapOption);

            addMapOption.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //viewMoreOption();

                    CaptureMapScreen();
                }
            });
            System.out.println("================================");


            System.out.println("notification type"  +UserNotification.getViewType());

//            if (UserNotification.getViewType().equals("ProfileViewTwo")) {
//                System.out.println("ProfileViewTwo" +"Mapfragment");
//                mapFragment = (SupportMapFragment) ((ProfileViewTwo) getActivity()).getSupportFragmentManager()
//                        .findFragmentById(R.id.mapFragment);
//                mapFragment.getMapAsync(this);
//
//            } else if (UserNotification.getViewType().equals("ProfileView")) {
//                System.out.println("ProfileView" +"MapFragmnt");
//                mapFragment = (SupportMapFragment) ((ProfileView) getActivity()).getSupportFragmentManager()
//                        .findFragmentById(R.id.mapFragment);
//                mapFragment.getMapAsync(this);
//
//            } else if (UserNotification.getViewType().equals("DashBoard")) {
//                System.out.println("==========================");
//                System.out.println("DashBoard" +"MapFragment");
//                mapFragment = (SupportMapFragment) ((Dashboard) getActivity()).getSupportFragmentManager()
//                        .findFragmentById(R.id.mapFragment);
//                mapFragment.getMapAsync(this);
//
//            } else if (UserNotification.getViewType().equals("PubDetailView")) {
//                System.out.println("PubdetailView" +"Map Fragment");
//                mapFragment = (SupportMapFragment) ((PubDetails) getActivity()).getSupportFragmentManager()
//                        .findFragmentById(R.id.mapFragment);
//                mapFragment.getMapAsync(this);

//            }

        }catch (NullPointerException ex){
            System.out.println("Null pointer exception");
        }



    }
    private void loadFriends() {
        friendList = new ArrayList<>();


//        loading.setVisibility(View.VISIBLE);

        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(getActivity(), "idprofile") + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i("Url", getString(R.string.server_url) + getString(R.string.friends_meth));
        JsonObjectRequest req = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.friends_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {

                            if (response.getString("data").contains("Acun amis"))
                                Toast.makeText(getActivity(), getString(R.string.no_friend), Toast.LENGTH_SHORT).show();
                            else {
                                if (response.getString("result").equals("true")) {
                                    JSONArray data = response.getJSONArray("data");
                                    Log.i("contacts list", String.valueOf(response));


                                    for (int i = 0; i < data.length(); i++) {
                                        try {
                                            JSONObject obj = data.getJSONObject(i);
                                            Log.i("contacts list elemts", String.valueOf(obj.getString("activeNewsFeed")));
//                                            friendList.add(new WISUser(obj.getInt("idprofile"), obj.getString("fullName"),"", obj.getString("photo"), obj.getInt("nbr_amis"),obj.getBoolean("activeNewsFeed")));
                                            friendList.add(new WISUser(Boolean.FALSE, obj.getString("blocked_by"), obj.getString("objectId"), obj.getString("activeNewsFeed"), obj.getInt("idprofile"), obj.getString("fullName"), obj.getString("photo"), obj.getInt("nbr_amis")));


                                        } catch (Exception e) {

                                        }
//

                                    }


                                    Map<String, List<WISUser>> map = new HashMap<String, List<WISUser>>();

                                    for (WISUser student : friendList) {
                                        String key = student.getFullName().substring(0, 1).toUpperCase();
                                        if (map.containsKey(key)) {
                                            List<WISUser> list = map.get(key);
                                            list.add(student);

                                        } else {
                                            List<WISUser> list = new ArrayList<WISUser>();
                                            list.add(student);
                                            map.put(key, list);
                                        }

                                    }

                                    Map<String, List<WISUser>> orderMap = new TreeMap<String, List<WISUser>>(map);


                                    Log.i("keys and values", String.valueOf(orderMap));

                                    Set<String> keyValues = orderMap.keySet();
                                    String[] arraykeys = keyValues.toArray(new String[keyValues.size()]);
                                    items = new ArrayList();

                                    for (int i = 0; i < orderMap.size(); i++) {

//

                                        if (orderMap.containsKey(arraykeys[i])) {
                                            String k = arraykeys[i];
//                                            header
                                            items.add(new SectionItem(k));


//                                            items

                                            for (WISUser user : map.get(k)) {

                                                items.add(new EntryItem(user));
                                            }


                                        }

                                    }


                                    Log.i("hash map key value", String.valueOf(items));

                                    setupPopUPView();


                                }
                            }
//                            loading.setVisibility(View.GONE);

                        } catch (JSONException e) {

//                            loading.setVisibility(View.GONE);
                            //if (getActivity() != null)
                            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                loading.setVisibility(View.GONE);
                //if (getActivity() != null)
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(req);
    }

    private void setupPopUPView() {

            popupList = new ArrayList<>();

            popupList.add(new Popupelements(R.drawable.copy_link, getString(R.string.screenShot)));
            popupList.add(new Popupelements(R.drawable.copy_chat, getString(R.string.wiscontact)));
            popupList.add(new Popupelements(R.drawable.share_profile, getString(R.string.actuality)));

            simpleAdapter = new Popupadapter(getActivity(), false, popupList);

        }


    private void showMapPermissionAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(false).setView(R.layout.custom_permission_alert);

        final AlertDialog alert = builder.create();
        alert.show();
        alert.findViewById(R.id.cancel_alert).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });
        alert.findViewById(R.id.accept_alert).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadFriends();
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean("locationPermissionDialogShown", true);
                editor.commit();
                alert.dismiss();
            }
        });

    }

    private void setListener() {


        addMapOption=(Button)view.findViewById(R.id.addMapOption);
//        loading=(ProgressBar)view.findViewById(R.id.mapProgress);

        try {

            view.findViewById(R.id.toggle).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((DrawerLayout)getActivity().findViewById(R.id.drawer_layout)).openDrawer(GravityCompat.START);
                }
            });
            addMapOption.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CaptureMapScreen();
                }
            });
        }catch (NullPointerException ex){
            System.out.println("Null pointer Exception" +ex.getMessage());
        }
    }


    private void CaptureMapScreen() {
        GoogleMap.SnapshotReadyCallback callback = new GoogleMap.SnapshotReadyCallback() {
            Bitmap bitmap;

            @Override
            public void onSnapshotReady(Bitmap snapshot) {
                // TODO Auto-generated method stub
                bitmap = snapshot;

                Log.d("map snap", String.valueOf(bitmap));

                snapShotFilePath = "/mnt/sdcard/"
                        + "MyMapScreen" + System.currentTimeMillis()
                        + ".jpg";

                try {
                    FileOutputStream out = new FileOutputStream( snapShotFilePath);

                    // above "/mnt ..... png" => is a storage path (where image will be stored) + name of image you can customize as per your Requirement

                    Log.e("mappath", String.valueOf(out));

//                    File file = new (filePath);

                    Toast toast= Toast.makeText(getActivity(),
                            (R.string.capture_success), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
                    toast.show();








                    Log.e("map filepath",snapShotFilePath);



                    bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);


//                    shareToAmis(view,bitmap);
                    viewMoreOption(bitmap);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        googleMap.snapshot(callback);


    }
    public void viewMoreOption(final Bitmap snapImageView) {


        DialogPlus dialog = DialogPlus.newDialog(getActivity())
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {


                        if (position == 0) {
                            dialog.dismiss();

                            shareToAmis(view, snapImageView);
//                            CaptureMapScreen();


                        } else if (position == 1) {
                            dialog.dismiss();


//                            shareLocationToWisActuality();


                        } else if (position == 2) {
                            dialog.dismiss();
                            Log.i("WIS chat", "chat click");


                        } else if (position == 3) {
                            dialog.dismiss();


                        }

                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L share dialog)
                .create();
        dialog.show();


    }

    public void shareToAmis(final View v, Bitmap snapedImage) {
        // StartAction


        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popup layout
        final View inflatedView = layoutInflater.inflate(R.layout.wis_amis_view, null, false);


        // get device size
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);
//        mDeviceHeight = size.y;


        // set height depends on the device size
        popWindow = new PopupWindow(inflatedView, size.x - 35, size.y - 300, true);

        popWindow.setTouchable(true);


        popWindow.setOutsideTouchable(false);

        popWindow.setAnimationStyle(R.style.animationName);


//        view.setAlpha(0.5f);


        popWindow.showAtLocation(v, Gravity.CENTER, 0, 80);


        close_btn = (ImageView) inflatedView.findViewById(R.id.close_btn);
        post_status = (Button) inflatedView.findViewById(R.id.post_status);
        snapShot = (ImageView) inflatedView.findViewById(R.id.snapShot);

        groupChatFriendList = (ListView) inflatedView.findViewById(R.id.groupfriendList);
        searchFilter = (EditText) inflatedView.findViewById(R.id.searchFilter);

        message = (TextView) inflatedView.findViewById(R.id.map_link);


//        message.setText(getMapUrlLink());
//
//        currentLocationName = getMapUrlLink();
//
//
//        loadFriendsList(groupChatFriendList);


        snapShot.setImageBitmap(snapedImage);


        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popWindow.dismiss();
//                view.setAlpha(1);
            }
        });


        post_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!message.getText().toString().equals("")) {
                    popWindow.dismiss();


                    selectFriendsAdapter.transferTextToContacts("map_link", String.valueOf(""), message.getText().toString());

//                    view.setAlpha(1);


                }


            }
        });


    }

    @Override
    public void onMapReady(GoogleMap Map) {
        googleMap = Map;

        if (UserNotification.getLocationSelected() == Boolean.TRUE) {
            System.out.println("from actuality");
            String add = UserNotification.getAddress();
            address = add;
            Log.e("address", address);
            Log.i("request url ", "http://maps.google.com/maps/api/geocode/json?address=" + address + "&sensor=false");
            DataLongOperationAsynchTask taks = new DataLongOperationAsynchTask();
            taks.execute(add);

        } else {

            System.out.println("current location");

            try {
                googleMap.setMyLocationEnabled(true);

            } catch (SecurityException ex) {
                System.out.println("Secure ex" + ex.getMessage());
            }
            googleMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
                @Override
                public void onMyLocationChange(Location location) {


                    try {

                        if (myLocation == null) {


                            myLocation = new LatLng(location.getLatitude(),
                                    location.getLongitude());


                            latitude = location.getLatitude();

                            langitude = location.getLongitude();

//                        currentLocationName = getMapUrlLink();


                            coordinates = String.valueOf(String.valueOf(latitude).concat(",").concat(String.valueOf(langitude)));

                            Log.i("current", coordinates);
                            System.out.println("------------- sttusdhjbs-------");
                            System.out.println(curretnLocationAdress);
                            System.out.println("------------- sttusdhjbs-------");


//changed

//                            googleMap.addMarker(new MarkerOptions().position(myLocation).title("je suis ici")).showInfoWindow();
//
////                        googleMap.moveCamera(CameraUpdateFactory.newLatLng(myLocation));
//
//                            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation,
//                                    14));
//
//
//                            getCityName(latitude, langitude);
//                            new UserNotification().setTopub(Boolean.TRUE);
//                            new UserNotification().setLattitude(latitude);
//                            new UserNotification().setLangtitude(langitude);
//
//
//                            ended

// create marker
                            MarkerOptions marker = new MarkerOptions().position(myLocation).title("Hello Maps");

// Changing marker icon
                            marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.location_red));

// adding marker
                            googleMap.addMarker(marker);

                            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation,
                                    14));


                            getCityName(latitude, langitude);
                            new UserNotification().setTopub(Boolean.TRUE);
                            new UserNotification().setLattitude(latitude);
                            new UserNotification().setLangtitude(langitude);


                        }
                    } catch (NullPointerException ex) {

                        ex.printStackTrace();

                    }
                }
            });
        }


//        googleMap.addMarker()

    }



    public static LatLng getLocationFromString(String address)
            throws JSONException {

        HttpGet httpGet = null;
        try {
            httpGet = new HttpGet(
                    "http://maps.google.com/maps/api/geocode/json?address="
                            + URLEncoder.encode(address, "UTF-8") + "&ka&sensor=false");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        HttpClient client = new DefaultHttpClient();
        HttpResponse response;
        StringBuilder stringBuilder = new StringBuilder();

        try {
            response = client.execute(httpGet);
            HttpEntity entity = response.getEntity();
            InputStream stream = entity.getContent();
            int b;
            while ((b = stream.read()) != -1) {
                stringBuilder.append((char) b);
            }
        } catch (ClientProtocolException e) {
        } catch (IOException e) {
        }

        JSONObject jsonObject = new JSONObject(stringBuilder.toString());

        double lng = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                .getJSONObject("geometry").getJSONObject("location")
                .getDouble("lng");

        double lat = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                .getJSONObject("geometry").getJSONObject("location")
                .getDouble("lat");

        return new LatLng(lat, lng);
    }


    public void getCityName(double latitude, double longitude) {

        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Please wait...");
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


        new UserLocation(getActivity(), new UserLocationListener() {
            @Override
            public void updateUserLocationInfo(String formatted_address) {

                dialog.dismiss();


                curretnLocationAdress = formatted_address;

                Log.e("return response", formatted_address);
            }
        }).execute(String.valueOf(latitude), String.valueOf(longitude));
    }

    private class DataLongOperationAsynchTask extends AsyncTask<String, Void, LatLng> {
        ProgressDialog dialog = new ProgressDialog(getActivity());

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.setMessage("Please wait...");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected LatLng doInBackground(String... params) {
            JSONObject response1;
            LatLng responsData = null;
            try {
                responsData = getLocationFromString(params[0]);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return responsData;

        }


        @Override
        protected void onPostExecute(LatLng result) {

            Log.e("resltaas", String.valueOf(result));





//                JSONObject jsonObject = new JSONObject(result);
//
//                lng = ((JSONArray)jsonObject.get("results")).getJSONObject(0)
//                        .getJSONObject("geometry").getJSONObject("location")
//                        .getDouble("lng");
//
//               lat = ((JSONArray)jsonObject.get("results")).getJSONObject(0)
//                        .getJSONObject("geometry").getJSONObject("location")
//                        .getDouble("lat");
//
//                Log.d("latitude", "" + lat);
//                Log.d("longitude", "" + lng);
//
//
//                Log.i("user address",address);



            latitude = result.latitude;

            langitude =result.longitude;

            getCityName(latitude,langitude);

//            final LatLng testAddr = new LatLng(lat,lng);




//                currentLocationName = getMapUrlLink();

            coordinates = String.valueOf(String.valueOf(latitude).concat(",").concat(String.valueOf(langitude)));

            googleMap.addMarker(new MarkerOptions().position(new LatLng(latitude,langitude)).title("je suis ici")).showInfoWindow();
//                googleMap.moveCamera(CameraUpdateFactory.newLatLng(testAddr));
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(result,
                    14));

            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
//    }
//
//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }
//
//    /**
//     * This interface must be implemented by activities that contain this
//     * fragment to allow an interaction in this fragment to be communicated
//     * to the activity and potentially other fragments contained in that
//     * activity.
//     * <p>
//     * See the Android Training lesson <a href=
//     * "http://developer.android.com/training/basics/fragments/communicating.html"
//     * >Communicating with Other Fragments</a> for more information.
//     */
//    public interface OnFragmentInteractionListener {
//        // TODO: Update argument type and name
//        void onFragmentInteraction(Uri uri);
//    }


}
