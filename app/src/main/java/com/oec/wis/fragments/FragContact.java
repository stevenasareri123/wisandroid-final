package com.oec.wis.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.InflateException;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.adapters.UserInvitAdapter;
import com.oec.wis.adapters.UserRInvitAdapter;
import com.oec.wis.dialogs.MyContact;
import com.oec.wis.entities.WISUser;
import com.oec.wis.tools.ListViewScrollable;
import com.oec.wis.tools.Tools;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FragContact extends Fragment {
    View view;
    ListViewScrollable lvContact, lvRequest;
    EditText etFilter;
    List<WISUser> users, rinvit;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            Tools.setLocale(getActivity(), Tools.getData(getActivity(), "lang_pr"));
            view = inflater.inflate(R.layout.frag_contact, container, false);


        } catch (InflateException e) {
        }
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initControls();
        loadUsers();
        setListener();
        loadInvits();
    }

    private void initControls() {
        lvContact = (ListViewScrollable) view.findViewById(R.id.lvContacts);
        lvContact.setExpanded(true);
        lvRequest = (ListViewScrollable) view.findViewById(R.id.lvInvits);
        lvRequest.setExpanded(true);
        etFilter = (EditText) view.findViewById(R.id.etFilter);
        Tools.hideKeyboard(getActivity());
    }


    private void setListener() {
        view.findViewById(R.id.toggle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DrawerLayout) getActivity().findViewById(R.id.drawer_layout)).openDrawer(GravityCompat.START);
            }
        });
        etFilter.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //final int DRAWABLE_LEFT = 0;
                //final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                //final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (etFilter.getRight() - etFilter.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        loadUsers();
                        Tools.hideKeyboard(getActivity());
                        return true;
                    }
                }
                return false;
            }
        });

        etFilter.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_SEARCH) {
                    loadUsers();
                    Tools.hideKeyboard(getActivity());
                }
                return true;
            }
        });

        view.findViewById(R.id.bMyContact).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), MyContact.class));
            }
        });
    }

    private void loadUsers() {
        users = new ArrayList<>();

        view.findViewById(R.id.loading).setVisibility(View.VISIBLE);

        final JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("id_profil", Tools.getData(getActivity(), "idprofile"));
            jsonBody.put("filtre", etFilter.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqNotifs = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.filterprofile_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.i("contact list url",getString(R.string.server_url) + getString(R.string.filterprofile_meth));
                            Log.i("contact parms", String.valueOf(jsonBody));

                            if (response.getBoolean("result")) {
                                JSONArray data = response.getJSONArray("data");
                                for (int i = 0; i < data.length(); i++) {
                                    try {
                                        JSONObject obj = data.getJSONObject(i);
                                        String name = "";
                                        try {
                                            name = obj.getString("firstname_prt") + " " + obj.getString("lastname_prt");
                                        } catch (Exception e) {
                                        }
                                        try {
                                            name += obj.getString("name");
                                        } catch (Exception e) {
                                        }
                                        users.add(new WISUser(obj.getInt("idprofile"), name, obj.getString("photo"), obj.getInt("nbr_amis_comm"), 0));
                                    } catch (Exception e) {
                                        String x = "";
                                    }
                                }
                                //if (users.size() > 0)
                                //view.findViewById(R.id.tvSuggetion).setVisibility(View.VISIBLE);
                                //else
                                //view.findViewById(R.id.tvSuggetion).setVisibility(View.GONE);
                                lvContact.setAdapter(new UserInvitAdapter(getActivity(), users));
                            }
                            view.findViewById(R.id.loading).setVisibility(View.GONE);

                        } catch (JSONException e) {
                            view.findViewById(R.id.loading).setVisibility(View.GONE);
                            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                view.findViewById(R.id.loading).setVisibility(View.GONE);
                //if (getActivity() != null)
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqNotifs);
    }

    private void loadInvits() {
        rinvit = new ArrayList<>();

        view.findViewById(R.id.loading).setVisibility(View.VISIBLE);

        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("id_profil", Tools.getData(getActivity(), "idprofile"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqNotifs = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.invits_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String x = "";
                            if (response.getBoolean("result")) {
                                JSONArray data = response.getJSONArray("data");
                                for (int i = 0; i < data.length(); i++) {
                                    try {
                                        JSONObject obj = data.getJSONObject(i);
                                        int nf = 0;
                                        try {
                                            //nf=obj.getInt("liens_amitie");
                                        } catch (Exception e) {
                                        }
                                        rinvit.add(new WISUser(obj.getInt("id_profil"), obj.getString("name"), obj.getString("photo"), nf, obj.getInt("lien_amitie")));
                                    } catch (Exception e) {
                                    }
                                }
                                //if (rinvit.size() > 0)
                                //view.findViewById(R.id.tvInvit).setVisibility(View.VISIBLE);
                                //else
                                //view.findViewById(R.id.tvInvit).setVisibility(View.GONE);
                                lvRequest.setAdapter(new UserRInvitAdapter(getActivity(), rinvit));
                            }
                            view.findViewById(R.id.loading).setVisibility(View.GONE);

                        } catch (JSONException e) {
                            view.findViewById(R.id.loading).setVisibility(View.GONE);
                            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                view.findViewById(R.id.loading).setVisibility(View.GONE);
                //if (getActivity() != null)
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqNotifs);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ApplicationController.getInstance().cancelPendingRequests(ApplicationController.TAG);
    }
}
