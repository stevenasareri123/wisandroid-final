package com.oec.wis;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.oec.wis.adapters.PopupadApterText;
import com.oec.wis.adapters.Popupadapter;
import com.oec.wis.entities.PopupElementsText;
import com.oec.wis.entities.Popupelements;
import com.oec.wis.entities.WISLanguage;
import com.oec.wis.tools.Tools;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnItemClickListener;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class Subscribe extends AppCompatActivity {
    Spinner spOrga, spLang;
    EditText etName, etEmail, etPwd;
    ImageView companyImageView,lanIV;
    TextView companyTextView,lanTextView;
    CircularImageView ivPhoto;
    String photoPath = "";
    int position;
    String spinner_item,selected;
    TextView spinTV;
    DialogPlus dialog;
    RelativeLayout companyRL,langRL;
    TextView headerTitleTV,companyTV,langTV;
    ImageView  companyIV,langIV;
    TextView tvPhone;
    Button b_subs;
   @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscribe);
        loadControls();
        loadSpinnerData();
        loadListener();
       setTypeFace();
   }


    private void loadControls() {
//        spOrga = (Spinner) findViewById(R.id.spOrga);
//        spLang = (Spinner) findViewById(R.id.spLang);
        etName = (EditText) findViewById(R.id.etName);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPwd = (EditText) findViewById(R.id.etPwd);
        ivPhoto = (CircularImageView) findViewById(R.id.ivPhoto);
        headerTitleTV=(TextView)findViewById(R.id.headerTitleTV);
        companyRL=(RelativeLayout)findViewById(R.id.companyRL);
        companyTV=(TextView)findViewById(R.id.companyTV);
        companyIV=(ImageView)findViewById(R.id.companyIV);
        langRL=(RelativeLayout)findViewById(R.id.langRL);
        langTV=(TextView)findViewById(R.id.langTV);
        langIV=(ImageView)findViewById(R.id.langIV);
        tvPhone=(TextView)findViewById(R.id.tvPhone);
        b_subs=(Button)findViewById(R.id.b_subs);

//        companyImageView=(ImageView)findViewById(R.id.companyImageView);
//        companyTextView=(TextView)findViewById(R.id.companyTV);
//        lanTextView=(TextView)findViewById(R.id.lanTV);
//        lanIV=(ImageView)findViewById(R.id.lanImageView);
//        companyRL=(RelativeLayout)findViewById(R.id.companyRL);
//        spinTV=(TextView)findViewById(R.id.spinnTV);
    }


    private void setTypeFace() {
        Typeface font=Typeface.createFromAsset(this.getAssets(),"fonts/Harmattan-R.ttf");
        headerTitleTV.setTypeface(font);
        etName.setTypeface(font);
        etEmail.setTypeface(font);
        etPwd.setTypeface(font);
        companyTV.setTypeface(font);
        langTV.setTypeface(font);
        tvPhone.setTypeface(font);
        b_subs.setTypeface(font);
    }

    private void loadSpinnerData() {

//company
        List<String> list = new ArrayList<String>();
        list.add("ENTERPRISE");
        list.add("PARTICULAR");
        list.add("ASSOCIATIONS/ORGANIZATIONS/OTHERS");
        list.add("COMPANY");
        final int listsize = list.size() - 1;


        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,R.layout.spinner_item, list) {
            @Override
            public int getCount() {
                return(listsize); // Truncate the list
            }
        };

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spOrga.setAdapter(dataAdapter);
//        spOrga.setSelection(listsize); // Hidden item to appear in the spinner


//end
//
        List<String> list1 = new ArrayList<String>();
        list1.add("FRENCH");
        list1.add("ENGLISH");
        list1.add("SPANISH");
        list1.add("LANGUAGE");
        final int listsize1 = list1.size() - 1;


        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(this,R.layout.new_spinner_item, list1) {
            @Override
            public int getCount() {
                return(listsize1); // Truncate the list
            }
        };

        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spLang.setAdapter(dataAdapter1);
//        spLang.setSelection(listsize1); // Hidden item to appear in the spinner

//



//        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this,
//                android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.orga_list));
//        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spOrga.setAdapter(dataAdapter);
//
//        dataAdapter = new ArrayAdapter<>(this,
//                android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.lang_list));
//        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spLang.setAdapter(dataAdapter);





//        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this,
//                R.layout.spinner_item, getResources().getStringArray(R.array.new_orga_list));
//        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spOrga.setAdapter(dataAdapter);
//
//        dataAdapter = new ArrayAdapter<>(this,
//                R.layout.spinner_item, getResources().getStringArray(R.array.lang_list));
//        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spLang.setAdapter(dataAdapter);


//
//        String companieslist = String.valueOf(spOrga.getSelectedItem());
//        if(companieslist.equals("COMPANY")){
//
//        }else
//        {


//        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this,
//                R.layout.spinner_item, getResources().getStringArray(R.array.new_orga_list));
//        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spOrga.setAdapter(dataAdapter);
//
//        System.out.println("position"  +dataAdapter.getPosition(String.valueOf(position)));
////
////        Spannable wordtoSpan = new SpannableString("Title");
////        wordtoSpan.setSpan(new ForegroundColorSpan(Color.WHITE), 0,
////                wordtoSpan.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
////        wordtoSpan.setSpan(new BackgroundColorSpan(Color.RED), 0,
////                wordtoSpan.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);SPAN_EXCLUSIVE_EXCLUSIVE
//
////        spOrga.setPrompt(wordtoSpan);
////
////
////        spOrga.setPrompt("COMPANY");
//        dataAdapter = new ArrayAdapter<>(this,
//                R.layout.spinner_item, getResources().getStringArray(R.array.new_lang_list));
//        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spLang.setAdapter(dataAdapter);
//        spOrga.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//
//            @Override
//            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
//                selected = spOrga.getSelectedItem().toString();
//                if (!selected.equals("COMPANY"))
//                    spinner_item = selected;
//                System.out.println(selected);
//
//                setid();
//            }
//
//            private void setid() {
////                sp.setSelection(sp_position);
////                spinTV.setText(spinner_item);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> arg0) {
//
//
//            }
//        });


    }

    private void loadListener() {
//        companyRL.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                openDropDown();
//            }
//        });
//        companyImageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//              openDropDown();
//            }
//        });

        langRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                langDropDown();
            }
        });
        langTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                langDropDown();
            }
        });
        langIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                langDropDown();
            }
        });
        companyRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                companyDropDown();
            }
        });
        companyTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                companyDropDown();
            }
        });
        companyIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                companyDropDown();
            }
        });
        findViewById(R.id.b_subs).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check()) {
                    switchActivity(position);

//                    switchActivity(companyTV.);

                }
            }
        });
        View.OnClickListener pickPhoto = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, 1);
            }
        };
        ivPhoto.setOnClickListener(pickPhoto);
        findViewById(R.id.tvPhone).setOnClickListener(pickPhoto);
    }
//        spLang.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                String lang = WISLanguage.fr_FR.toString();
//                if (spLang.getSelectedItemPosition() == 1)
//                    lang = WISLanguage.en_US.toString();
//                else if (spLang.getSelectedItemPosition() == 2)
//                    lang = WISLanguage.es_ES.toString();
//                Tools.setLocale(Subscribe.this, lang.substring(0, lang.indexOf("_")));
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//
//
//            }
//        });
//    }

    private void langDropDown() {

        final ArrayList popupList = new ArrayList();

        popupList.add(new PopupElementsText("FRENCH"));
        popupList.add(new PopupElementsText("ENGLISH"));
        popupList.add(new PopupElementsText("SPANISH"));

        PopupadApterText simpleAdapter = new PopupadApterText(this, false, popupList);


        dialog = DialogPlus.newDialog(this)
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View v, int position) {
                        if(position ==0){
                            dialog.dismiss();

                            langTV.setText("FRENCH");
                            langTV.setTextColor(getResources().getColor(R.color.black));
                        }
                        else if(position ==1){
                            dialog.dismiss();
                            langTV.setText("ENGLISH");
                            langTV.setTextColor(getResources().getColor(R.color.black));
                        }
                        else if(position == 2) {
                            dialog.dismiss();
                            langTV.setText("SPANISH");
                            langTV.setTextColor(getResources().getColor(R.color.black));

                        }

                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L share dialog)
                .create();
        dialog.show();
    }


    private void companyDropDown() {
        final ArrayList popupList = new ArrayList();

        popupList.add(new PopupElementsText("ENTERPRISE"));
        popupList.add(new PopupElementsText("PARTICULAR"));
        popupList.add(new PopupElementsText("ASSOCIATIONS/ORGANIZATIONS/OTHERS"));

        PopupadApterText simpleAdapter = new PopupadApterText(this, false, popupList);


        dialog = DialogPlus.newDialog(this)
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View v, int position) {
                        if(position ==0){
                            dialog.dismiss();

                            companyTV.setText("ENTERPRISE");
                            companyTV.setTextColor(getResources().getColor(R.color.black));


                        }
                        else if(position ==1){
                            dialog.dismiss();
                            companyTV.setText("PARTICULAR");
                            companyTV.setTextColor(getResources().getColor(R.color.black));


                        }
                        else if(position == 2) {
                            dialog.dismiss();
                            companyTV.setText("ASSOCIATIONS/ORGANIZATIONS/OTHERS");
                            companyTV.setTextColor(getResources().getColor(R.color.black));


                        }

                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L share dialog)
                .create();
        dialog.show();

    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch (requestCode) {
            case 1:
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = imageReturnedIntent.getData();
                    InputStream imageStream = null;
                    try {
                        imageStream = getContentResolver().openInputStream(selectedImage);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    Bitmap sImage = BitmapFactory.decodeStream(imageStream);
                    ivPhoto.setImageBitmap(sImage);

                    final String[] proj = {MediaStore.Images.Media.DATA};
                    final Cursor cursor = managedQuery(selectedImage, proj, null, null,
                            null);
                    final int column_index = cursor
                            .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    cursor.moveToLast();
                    photoPath = cursor.getString(column_index);
                }
                break;
            default:
                break;
        }
    }



    private void switchActivity(int position) {
        Intent i = null;
        String lang = WISLanguage.fr_FR.toString();
//        if (spLang.getSelectedItemPosition() == 1)
//            lang = WISLanguage.en_US.toString();
//        else if (spLang.getSelectedItemPosition() == 2)
//            lang = WISLanguage.es_ES.toString();
        switch (position) {
            case 0:
                i = new Intent(getApplicationContext(), SubsCampany.class);
                break;
            case 1:
                i = new Intent(getApplicationContext(), SubsParticular.class);
                break;
            case 2:
                i = new Intent(getApplicationContext(), SubsAsso.class);
                break;
        }
        if (i != null) {
            i.putExtra("name", etName.getText().toString());
            i.putExtra("email", etEmail.getText().toString());
            i.putExtra("pwd", etPwd.getText().toString());
            i.putExtra("lang", lang);
            i.putExtra("photo", photoPath);
            startActivity(i);
        }
    }
//    private void switchActivity(int orga) {
//        Intent i = null;
//        String lang = WISLanguage.fr_FR.toString();
//        if (spLang.getSelectedItemPosition() == 1)
//            lang = WISLanguage.en_US.toString();
//        else if (spLang.getSelectedItemPosition() == 2)
//            lang = WISLanguage.es_ES.toString();
//        switch (orga) {
//            case 0:
//                i = new Intent(getApplicationContext(), SubsCampany.class);
//                break;
//            case 1:
//                i = new Intent(getApplicationContext(), SubsParticular.class);
//                break;
//            case 2:
//                i = new Intent(getApplicationContext(), SubsAsso.class);
//                break;
//        }
//        if (i != null) {
//            i.putExtra("name", etName.getText().toString());
//            i.putExtra("email", etEmail.getText().toString());
//            i.putExtra("pwd", etPwd.getText().toString());
//            i.putExtra("lang", lang);
//            i.putExtra("photo", photoPath);
//            startActivity(i);
//        }
//    }

    private boolean check() {
        boolean check = true;
        if (TextUtils.isEmpty(etName.getText().toString())) {
            check = false;
            etName.setError(getString(R.string.msg_empty_name));
        }
        if (TextUtils.isEmpty(etEmail.getText().toString())) {
            check = false;
            etEmail.setError(getString(R.string.msg_empty_email));
        } else if (!Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString()).matches()) {
            check = false;
            etEmail.setError(getString(R.string.msg_invalid_email));
        }
        if (TextUtils.isEmpty(etPwd.getText().toString())) {
            check = false;
            etPwd.setError(getString(R.string.msg_empty_pwd));
        }
        return check;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ApplicationController.getInstance().cancelPendingRequests(ApplicationController.TAG);
    }
}
